
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinoViaje } from './destino-viaje.model';
import { HttpClientModule } from '@angular/common/http';
import { isNgTemplate } from '@angular/compiler';

export interface DestinosViajesState {
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
}

export function initializeDestinoViajesState() {
    return {
        items: [],
        loading: false,
        favorito: null
    };
}

export enum DestinoViajesActionTypes {
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
    VOTE_UP = '[Destinos Viajes] Vote Up',
    VOTE_DOWN = '[Destinos Viajes] Vote Down',
    INIT_MY_DATA = '[Destinos Viajes]Init My Data'
}

export class NuevoDestinoAction implements Action {
    type = DestinoViajesActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje) { }
}

export class VoteUpAction implements Action {
    type = DestinoViajesActionTypes.VOTE_UP;
    constructor(public destino: DestinoViaje) { }
}

export class VoteDownAction implements Action {
    type = DestinoViajesActionTypes.VOTE_DOWN;
    constructor(public destino: DestinoViaje) { }
}
export class InitMyDataAction implements Action {
    type = DestinoViajesActionTypes.INIT_MY_DATA;
    constructor(public destinos: string[]) {}
}

export class ElegidoFavoritoAction implements Action {
    type = DestinoViajesActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoViaje) { }
}
export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction | VoteUpAction | VoteDownAction | InitMyDataAction;

export function reducerDestinosViajes(
    state: DestinosViajesState,
    action: DestinosViajesActions,
): DestinosViajesState {
    switch (action.type) {
        case DestinoViajesActionTypes.INIT_MY_DATA: {
            const destinos: string[] = (action as InitMyDataAction).destinos;
            return {
                ...state,
                items: destinos.map((d) => new DestinoViaje(d, ''))
            }
        };
        case DestinoViajesActionTypes.NUEVO_DESTINO: {
            return {
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino]
            };
        }
        case DestinoViajesActionTypes.ELEGIDO_FAVORITO: {
            state.items.forEach(x => x.setSelected(false));
            const fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
            fav.setSelected(true);
            return {
                ...state,
                favorito: fav
            };
        }
        case DestinoViajesActionTypes.VOTE_UP: {
            const d: DestinoViaje = (action as VoteUpAction).destino;
            d.voteUp();
            return { ...state };
        };
        case DestinoViajesActionTypes.VOTE_DOWN: {
            const d: DestinoViaje = (action as VoteDownAction).destino;
            d.voteDown();
            return { ...state };
        };
    }
    return state;
}



@Injectable()
export class DestinoViajesEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.action$.pipe(
        ofType(DestinoViajesActionTypes.NUEVO_DESTINO),
        map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );
    constructor(private action$: Actions) { }
}




